package fr.mipn.anagrams.tests

import utest._

import fr.mipn.anagrams.Bag
// import fr.mipn.anagrams.{Lexicon,Anagrams}

object TestsAnagrams extends TestSuite{
  val tests = Tests{
    test ("Bag equality 1") {
        (Bag("aab") == Bag("aba")) && (Bag("aab") != Bag("ab"))
    }
    test ("Bag inequality") {
      Bag("aab") <= Bag("aabbcc")
    }
    test ("Bag substraction") {
      Bag("aab") - Bag("bbc") == Bag("aa")
    }
  /* val a = new Anagrams(new Lexicon("lexique.txt")) 
    test ("Find one word anagrams of vélo") {
      a.oneWord("vélo").map(_.toSet) == Set(Set("vélo", "volé", "lové"))
    }
    test ("Find 2 words anagrams of vélo volé") {
      println(a.twoWords("vélo volé").to(List).map(_.toSet))
      a.twoWords("vélo volé").map(_.map(_.toSet)) == Set(List(Set("vélo", "volé", "lové"), Set("vélo", "volé", "lové")))
    }
    test ("Find 3 words anagrams of vélo volé volé") {
      a.threeWords("vélo volé volé").map(_.map(_.toSet)) == Set(List(Set("vélo", "volé", "lové"), Set("vélo", "volé", "lové"), Set("vélo", "volé", "lové")))
    }
    test ("Find all anagrams of vélo") {
      a.allSolutions("vélo").map(_.map(_.toSet)) == LazyList(List(Set("vélo", "volé", "lové")))
    }
    test ("Find all anagrams of vélo volé") {
      a.allSolutions("vélo volé").map(_.map(_.toSet)) == LazyList(List(Set("vélo", "volé", "lové"), Set("vélo", "volé", "lové")))
    }
    test ("Find all anagrams of vélo volé volé") {
      a.allSolutions("vélo volé volé").map(_.map(_.toSet)) == LazyList(List(Set("vélo", "volé", "lové"), Set("vélo", "volé", "lové"), Set("vélo", "volé", "lové")))
    }
    test ("Find all anagrams of vélo volé volé volé") {
      a.allSolutions("vélo volé volé volé").map(_.map(_.toSet)) == LazyList(List(Set("vélo", "volé", "lové"), Set("vélo", "volé", "lové"), Set("vélo", "volé", "lové"), Set("vélo", "volé", "lové")))
    } */
  } 
}