package fr.mipn.anagrams

/* les multiensembles de lettres */
class Bag(val m: Map[Char, Int]) {
  def <= (y: Map[Char, Int]): Boolean =
    m.keys.forall( (a: Char) => m(a) <= y.getOrElse(a, 0))
  def <= (y: Bag): Boolean = this <= y.m
 
  def -(y: Map[Char, Int]): Bag = Bag(m.map( {case (a: Char, n : Int) => (a, n - y.getOrElse(a, 0))}).filter({case (a, n) => n > 0}))
  def -(y: Bag): Bag = this - y.m

  def length: Int =
    m.keys.foldLeft(0)( (sum: Int, a: Char) => m(a) + sum )

  def canEqual(a: Any) = a.isInstanceOf[Bag]
  override def equals(that: Any): Boolean = that match {
    case that: Bag => that.canEqual(this) &&
      this.hashCode == that.hashCode
    case _ => false
  }
  override def toString(): String = {
    val s = m.to(List).sortBy(_._1).map({case (c: Char, n: Int) => c.toString * n}).foldLeft("")({case (acc: String, letters: String) => acc + letters})
    s"Bag(${s})"
  }

  override def hashCode: Int = m.hashCode
}

object Bag {
  def apply(m: Map[Char, Int]) = new Bag(m)
  def apply(s: String) = new Bag(split(s))
  def apply() = new Bag(Map())
  def split(word: String): Map[Char,Int] = word.filterNot(_ == ' ').toList.groupBy((x) => x).view.mapValues(_.length).toMap 
}

/* - le lexique doit être lu à partir du dossier resources, 
   - on va paramétrer sa création par le nom du fichier du lexique 
   - il doit être organisé en mettant ensemble les mots qui utilisent les mêmes lettres
   - il doit permettre la recherche
    - pour des raisons d'optimisation on veut parcourir les clés de celle qui est représentée
      par la chaîne de caractéres la plus longue à la plus petite.
   => ce sera un Map[Bag,List(String)].
*/
class Lexicon(filename: String) {
  private def readLexicon() = { 
    import scala.io.Source
    val buffered = Source.fromResource(filename)
    val lexicon = buffered.getLines().toList
    buffered.close() 
    lexicon
  }
  lazy val lexicon = readLexicon()  
  lazy val table = lexicon.groupBy(Bag.apply)
  lazy val keys = table.keys.toVector.sortBy(_.toString).sortBy((token) => -token.length)
}

class Anagrams(lexicon: Lexicon) {

  def simplify[A](solutions:Vector[List[A]]): Set[List[A]] = {
     // solutions.map(_.groupBy((x:A) => x).keys.to(List)).to(Set)
     // solutions.to(Set)
     solutions.map(_.sortBy(_.toString)).toSet
  }

  def oneWord(letters: Bag): Set[List[String]] = Set(lexicon.table.getOrElse(letters, List())) 

  def twoWords(letters: Bag): Set[List[List[String]]] = simplify {
    for {
        key1 <- lexicon.keys
        if (key1 <= letters)
        word1 <- lexicon.table.get(key1)
        word2 <- lexicon.table.get(letters - key1)
      } yield List(word1, word2)
  } 
  def threeWords(letters: Bag): Set[List[List[String]]] = simplify {
    for {
      key1 <- lexicon.keys
      if (key1 <= letters)
      word1 <- lexicon.table.get(key1).to(List)
      remain = letters - key1
      key2 <- lexicon.keys
      if (key2 <= remain)
      word2 <- lexicon.table.get(key2).toList  
      key3 =  remain - key2
      word3 <- lexicon.table.get(key3).toList 
    } yield List(word1, word2, word3)
  }
  
  /* MÉFI */
  def allSolutions(x: Bag, firstWordMinLength: Int = 3): LazyList[List[List[String]]] = decompose(x, lexicon.keys.to(LazyList), 0, firstWordMinLength)
    .map(_.map(bag => lexicon.table.getOrElse(bag, List.empty)))
  private def decompose(x: Bag, tokens: LazyList[Bag], max_length: Int = 0, firstWordMinLength: Int = 4): LazyList[List[Bag]] = {
    // debug println(s"decompose: x = ${x}, tokens = ${tokens.to(List)}, ${max_length}, ${firstWordMinLength}")
    if (x <= Bag()) LazyList(List()) 
    else {
      var filtered_tokens = tokens.filter(_ <= x)
      val first_tokens = if (max_length == 0) filtered_tokens.filter(_.length >= firstWordMinLength) else filtered_tokens
      first_tokens.flatMap {
        (token) => if (((0 == max_length) || (token.length <= max_length)) && (token <= x)) {
              decompose(x - token, filtered_tokens.dropWhile(_ != token), token.length, 0).map(token::_)
          } 
          else LazyList()
      }
    }
  }

  def oneWord(s: String): Set[List[String]] = oneWord(Bag(s))
  def twoWords(s: String): Set[List[List[String]]] = twoWords(Bag(s))
  def threeWords(s: String): Set[List[List[String]]] = threeWords(Bag(s))
  def allSolutions(s: String): LazyList[List[List[String]]] = allSolutions(Bag(s))
  def allSolutions(s: String, min: Int): LazyList[List[List[String]]] = allSolutions(Bag(s), min)
}

object Main extends App {

  /* exercice préliminaire 
  val tokens = List(5,2)

  def decomposeInt(x: Int): List[List[Int]] = { 
  if (x == 0) List(List()) 
    else  tokens.flatMap {
      (token) => if (x >= token) decomposeInt(x - token).map(token::_) else List() 
    }
  }
  println(decomposeInt(10))
  */

  val français = new Lexicon("lexique.txt")
  val a = new Anagrams(français)

  def displayResults(xss: List[List[String]]): Unit = {
    (List("☞")::xss).map { // 
        (xs) => 
        val width = xs.map(_.length).max + 1
        xs.map(_.padTo(width, ' ')) 
      }.foldRight(List("")){
          (xs,ys)=>
            val widthxs = xs.map(_.length).max
            val widthys = ys.map(_.length).max

            xs.zipAll(ys,"".padTo(widthxs, ' '),"".padTo(widthys, ' ')).map((pair) => pair._1 + pair._2)
        }.foreach(println)
  }
  
/* interface utilisateur minimaliste */
  var cont = true
  while (cont) {
    println("Entrez votre phrase")
    val target = scala.io.StdIn.readLine()
    if ("" == target) {
     cont = false
    } else {
      println(s"vous avez entré $target")
      val t0 = System.nanoTime() // 
      val letters = Bag(target)

      println(s"anagrammes de ${target} en un seul mot :")
      a.oneWord(letters).foreach(_.foreach(println)) 
      
      println(s"Ensemble des anagrammes de ${target} de deux mots :")
      a.twoWords(letters).foreach(displayResults)

      println(s"Ensemble des anagrammes de ${target} de trois mots :")
      val three = a.threeWords(letters)
      three.foreach(displayResults)
      val t1 = System.nanoTime()
      println("Temps écoulé: " + (t1 - t0) + "ns")

      println("Recherche de tous les anagrammes, affichage incrémental (cela peut prendre beaucoup de temps)")
      val all = a.allSolutions(letters)
      all.foreach(displayResults)
      println("Ensemble simplifié des solutions")
      a.simplify(all.to(Vector)).foreach(displayResults)
      val t2 = System.nanoTime()
      println("Temps écoulé: " + (t2 - t1) + "ns")      
      println("Temps total écoulé: " + (t2 - t0) + "ns")      
    }
  }
}
